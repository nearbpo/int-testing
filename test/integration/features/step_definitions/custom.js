var apickli = require('apickli');

module.exports = function () {
    this.Given(/^I set tracing header$/, function (callback) {
        this.apickli.addRequestHeader('X-Tracing-Id', '123');
        callback();
      });

    this.Given(/^I set context (.*)$/, function (context, callback) {
        context = this.apickli.replaceVariables(context);
        this.apickli.storeValueInScenarioScope('appName', context + 'algo');
        callback();
    });
}