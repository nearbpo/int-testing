const apickli = require('apickli');
const fs = require('fs');
const jsonPath = require('JSONPath');
const config = require('../../config.json');


module.exports = function () {
    this.Given(/^I have an access_token for developer app (.*)$/, function(appName, callback){
        try {
            var app = this.apickli.replaceVariables(appName);
            var appObject = this.apickli.scenarioVariables['applications'][app];
            var key = appObject.credentials[appObject.credentials.length-1].consumerKey;
            var secret = appObject.credentials[appObject.credentials.length-1].consumerSecret;

            var scheme = this.apickli.scenarioVariables['tokenScheme'];
            var domain = this.apickli.scenarioVariables["tokenDomain"];
            var basepath = this.apickli.scenarioVariables["tokenBasepath"];

            var callout = new apickli.Apickli(scheme, domain + basepath);
            callout.addHttpBasicAuthorizationHeader(key, secret);
            callout.addRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            callout.setRequestBody("grant_type=client_credentials&scope=READ");

            var self = this;

            callout.post("/token", function (error, response) {
                if (error) {
                    callback(new Error(error));
                }
                var token = JSON.parse(response.body).access_token;
                self.apickli.scenarioVariables["bearer_access_token"] = 'Bearer ' + token;
                callback();
            });

        } catch (e) {
            callback(e);
        }
    });

}