const apickli = require('apickli');
const fs = require('fs');
const jsonPath = require('JSONPath');
// const config = require('../../config.json');


module.exports = function () {
    this.Given(/^I load credentials from developer app (.*) to variables (.*) and (.*)$/, function(appName, consumerKey, consumerSecret, callback){
        try {
            var app = this.apickli.replaceVariables(appName);
            consumerKey=this.apickli.replaceVariables(consumerKey);
            consumerSecret=this.apickli.replaceVariables(consumerSecret);
            var appObject = this.apickli.scenarioVariables['applications'][app];
            var key = appObject.credentials[appObject.credentials.length-1].consumerKey;
            var secret = appObject.credentials[appObject.credentials.length-1].consumerSecret;

            console.log(consumerKey + '=' + key);
            console.log(consumerSecret + '=' + secret);
            this.apickli.storeValueInScenarioScope(consumerKey, key);
            this.apickli.storeValueInScenarioScope(consumerSecret, secret);
            callback();
        } catch (e) {
            callback(e);
        }
    });

    this.Given(/^I have basic authentication credentials from developer app (.*)$/, function(appName, callback){
        try {
            var app = this.apickli.replaceVariables(appName);
            var appObject = this.apickli.scenarioVariables['applications'][app];
            var username = appObject.credentials[appObject.credentials.length-1].consumerKey;
            var password = appObject.credentials[appObject.credentials.length-1].consumerSecret;
            this.apickli.addHttpBasicAuthorizationHeader(username, password);
            callback();
        } catch (e) {
            callback(e);
        }
    });
    this.Given(/^I set variable (.*) to value of path (.*) in (.*) developer app$/, function (variable, path, app, callback) {
        try {
            variable = this.apickli.replaceVariables(variable);
            path = this.apickli.replaceVariables(path);
            app = this.apickli.replaceVariables(app);

            var appObject = this.apickli.scenarioVariables['applications'][app];
            var result = jsonPath({json: appObject, path: path, resultType: 'value', autostart: true});
            this.apickli.storeValueInScenarioScope(variable, result[0]);
            callback();
        } catch (e) {
            callback(e);
        }
    });

    this.Given(/^I source variables from path (.*) in (.*) developer app$/, function (path, app, callback) {
        try {
            path = this.apickli.replaceVariables(path);
            app = this.apickli.replaceVariables(app);
            var appObject = this.apickli.scenarioVariables['applications'][app];
            var result = jsonPath({json: appObject, path: path, resultType: 'value', autostart: true});

            Object.keys(result).forEach(key => {
                this.apickli.storeValueInScenarioScope(key, result[key]);
            })
            callback();
        } catch (e) {
            callback(e);
        }
    });

    this.Given(/^I source variables from path (.*) in (.*) developer app using prefix (.*)$/, function (path, app, prefix, callback) {
        try {
            path = this.apickli.replaceVariables(path);
            app = this.apickli.replaceVariables(app);
            prefix = this.apickli.replaceVariables(prefix);

            var appObject = this.apickli.scenarioVariables['applications'][app];
            var result = jsonPath({json: appObject, path: path, resultType: 'value', autostart: true});

            Object.keys(result).forEach(key => {
                this.apickli.storeValueInScenarioScope(prefix + '.' + key, result[key]);
            })
            callback();
        } catch (e) {
            callback(e);
        }
    });
}