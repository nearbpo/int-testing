@api
Feature: API proxy test

    # @dev
    # @qa
    # @prd
    # Scenario: Confirm username value is in response payload
    #     Given I set X-Secure-Header header to secure-value
    #         And I set X-User-Tester header to ${user.name}
    #         And I set X-Param-Header header to `domain`
    #         And I set tracing header
    #         # And I print scenario variables
    #     When I GET /
    #     Then response code should be 200
    #         And response body path $.username should be ${user.name}

    # @dev
    # Scenario Outline: Test anything endpoint
    #     Given I set X-Secure-Header header to <x-secure-header-value>
    #     When I POST to /
    #     Then response body path $.headers.X-Secure-Header should be <x-secure-header-value>
    #     Examples:
    #         | x-secure-header-value |
    #         | valor1                |
    #         | valor2                |

    # @dev
    # Scenario Outline: Test greeting
    #     Given I set context <context>
    #         And I set Content-Type header to <appjson>
    #         And I set X-App-Name header to <appName>
    #         And I pipe contents of file <fixture> to body
    #     When I POST to /
    #     Then response code should be 200
    #     Examples:
    #         | context  | appjson          |  appName  | fixture                                             |
    #         | app1     | application/json | `appName` | ./target/test/integration/features/fixtures/greeting.json  |
    #         | app2     | application/json | `appName` | ./target/test/integration/features/fixtures/greeting.json  |
    
    @dev
    Scenario: Test loading developer app properties
        Given I have basic authentication credentials from developer app ${proxy.name}-developerApp
            And I set form parameters to
                | parameter      | value              |
                | grant_type     | client_credentials |
                | scope          |                    |
            And I print scenario variables
        When I POST to /token
        Then response code should be 200
            And response body should contain access_token
            And I print response body

