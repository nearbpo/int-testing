'use strict'

var sinon = require('sinon');
var Url = require('url-parse');


var contextGetVariableMethod,
    contextSetVariableMethod,
    contextProperties,
    httpClientSendMethod,
    httpClientGetMethod,
    requestConstructor,
    policyProperties,
    printMethod,
    exhangeObj;

beforeEach(function() {

    global.context = {
        getVariable: function(s) {},
        setVariable: function(a, b) {},
        setProperties: function(props) {
            Object.assign(global.context, props);
        },

    };

    global.properties = {
        setProperties: function(props) {
            Object.assign(global.properties, props);
        }
    };

    global.httpClient = {
        send: function() {
        },
        get: function() {
        }
    };

    global.Request = function(s) {
        var props = ['url', 'method', 'headers', 'body']
        for (var i = 0; i < arguments.length; i++) {
            this[props[i]] = arguments[i];
        }
        var url = new Url(this.url, true);
        this.protocol = url.protocol;
        this.host = url.host;
        this.port = url.port;
        this.path = url.pathname;
        this.queryParams = url.query;
        this.uri = this.url;
    };

    global.print = function (s) {
        console.log(s);
    };


    contextGetVariableMethod = sinon.stub(global.context, 'getVariable');
    contextSetVariableMethod = sinon.spy(global.context, 'setVariable');
    contextProperties = sinon.spy(global.context, 'setProperties');
    httpClientSendMethod = sinon.stub(httpClient, 'send');
    httpClientGetMethod = sinon.stub(httpClient, 'get');
    requestConstructor = sinon.spy(global, 'Request');
    printMethod = sinon.spy(global, 'print');
    policyProperties = sinon.spy(global.properties, 'setProperties');

});

afterEach(function() {
    contextGetVariableMethod.restore();
    contextSetVariableMethod.restore();
    contextProperties.restore();
    httpClientSendMethod.restore();
    httpClientGetMethod.restore();
    requestConstructor.restore();
    policyProperties.restore();
});

exports.getMock = function() {
    return {
        contextGetVariableMethod: contextGetVariableMethod,
        contextSetVariableMethod: contextSetVariableMethod,
        withContextProperties: contextProperties,
        httpClientSendMethod: httpClientSendMethod,
        httpClientGetMethod: httpClientGetMethod,
        requestConstructor: requestConstructor,
        withPolicyProperties: policyProperties,
        printMethod: printMethod,
    };
}
