#!/usr/bin/env bash

option=${1:-run}
echo $option

function print_marker() {
  echo "@@@@@@@@@----------------------------------------------- $1 -----------------------------------------------@@@@@@@@@"
}

while true; do
    case $option in
        run )

          print_marker "process resources"
          mvn clean process-resources;

          lines=$(find target/test/unit -maxdepth 1 -name "*.js" | wc -l);
          echo $lines
          if [ $lines -gt 0 ]; then
            print_marker "mocha tests found"
            npm run-script mocha;
          else
            print_marker "no mocha tests found"
          fi;

          print_marker "creating dependencies"
          mvn clean process-resources \
              apigee-config:apiproducts  \
              apigee-config:developers \
              apigee-config:apps \
              apigee-config:exportAppKeys \
              -Dapigee.config.exportDir=./target/test \
              -Dapigee.config.options=update;

              print_marker "running apickli"
              #npm run-script apickli;
          break;;

        clean )
          mvn clean process-resources \
              apigee-config:apps \
              apigee-config:developers \
              apigee-config:apiproducts  \
              -Dapigee.config.options=delete
              rm -f ./target/test/devAppKeys.json
          exit;;
        * ) echo "Invalid option. Options allowed are run or clean. Bye"; exit;;
    esac
done

#mvn clean process-resouces