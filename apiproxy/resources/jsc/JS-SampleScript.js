var payload = JSON.parse(context.getVariable('response.content'));

payload.username = '${user.name}';
print('--------------------------');
print(JSON.stringify(payload, null, 4));
print('--------------------------');

context.setVariable('response.content', JSON.stringify(payload, null, 4));