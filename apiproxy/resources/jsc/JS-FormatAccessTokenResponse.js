'use strict'
/* globals context */

var accessToken = JSON.parse(context.getVariable('response.content'));

var responseContent = {
    "token_type": "Bearer",
    "expires_in": parseInt(accessToken.expires_in),
    "issued_at": accessToken.issued_at,
    "client_id": accessToken.client_id,
    "access_token": accessToken.access_token,
    "application_name": accessToken.application_name,
    "scope": accessToken.scope
};

context.setVariable('response.content', JSON.stringify(responseContent, null, 4));