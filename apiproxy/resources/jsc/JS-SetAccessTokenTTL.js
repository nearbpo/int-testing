'use strict'
/* globals context */

var accessTokenTTL = context.getVariable('verifyapikey.VerifyAPIKey.accessTokenTTLSeconds');
if (!accessTokenTTL) {
    accessTokenTTL = context.getVariable('verifyapikey.VerifyAPIKey.apiproduct.accessTokenTTLSeconds');
}
context.setVariable('flow.accessTokenTTL', accessTokenTTL ? (accessTokenTTL * 1000) + '' : '3600000');